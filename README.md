# synonyms

Application was implemented using .Net Core 2.2 and Asp.Net MVC. It was configured with docker-compose so to deploy the application run `docker-compose up` in the main application folder. Address bound by default is `https://localhost:8080` Database schema should be created automatically, and some seeding data should be added. During implementation I have focused on backend, so frontend is as simple as possible.

#assumptions

* I have assumed that amount of data is large enough that looking through comma separated lists of synonyms to allow for bidirectionality has significant performance impact.
* I have assumed that data model should be implemented _exactly_ as shown in the task description. Otherwise, previous point could be resolved by changing data model.
* I have assumed that amount of data is too large to fit in cache fully, nut not large enough that it cannot be cached effectively.
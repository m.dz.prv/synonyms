﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace synonyms_demo.Migrations
{
    public partial class IndexCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_SynonymEntries_Term",
                table: "SynonymEntries",
                column: "Term");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SynonymEntries_Term",
                table: "SynonymEntries");
        }
    }
}

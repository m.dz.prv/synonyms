﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace synonyms_demo.Migrations
{
    public partial class DataSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "SynonymEntries",
                columns: new[] { "Id", "Synonyms", "Term" },
                values: new object[,]
                {
                    { 1, "laptop,notebook", "computer" },
                    { 2, "notebook,pc", "computer" },
                    { 3, "workstation", "computer" },
                    { 4, "computer,portable-computer", "notebook" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SynonymEntries",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "SynonymEntries",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "SynonymEntries",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "SynonymEntries",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}

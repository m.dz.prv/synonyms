﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace synonyms_demo.Models
{
    public class SynonymViewModel
    {
        public string Term { get; set; }
        public HashSet<string> Synonyms { get; set; } = new HashSet<string>();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace synonyms_demo.Models
{
    public class SynonymEntryModel
    {
        public int Id { get; set; }
        public string Term { get; set; }
        public string Synonyms { get; set; }
    }
}

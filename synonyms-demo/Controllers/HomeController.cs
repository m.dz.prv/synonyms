﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using synonyms_demo.Models;

namespace synonyms_demo.Controllers
{
    public class HomeController : Controller
    {
        private SynonymsDbContext DbContext { get; set; }
        private IMemoryCache Cache { get; set; }

        public HomeController(SynonymsDbContext dbContext, IMemoryCache cache)
        {
            this.DbContext = dbContext;
            this.Cache = cache;
        }

        [HttpGet("/")]
        public IActionResult Index()
        {
            return View();
        }

        
        [HttpGet("/synonyms")]
        public async Task<IActionResult> Synonyms([Required] string term)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var synonyms = new HashSet<string>();
            if (!Cache.TryGetValue(term, out synonyms))
            {
                var synonymDbEntries = await DbContext.SynonymEntries.Where(x => x.Term == term).ToListAsync();
                //contains in this query is translated to CHARINDEX in SQL with significant impact on performance
                var reverseSynonyms = await DbContext.SynonymEntries.Where(x => x.Synonyms.Contains(term)).Select(x => x.Term).ToListAsync();
                synonyms = synonymDbEntries.SelectMany(x => x.Synonyms.Split(',')).Concat(reverseSynonyms).ToHashSet(); //get unique synonymous words

                //cache expiration time should be chosen based on available resources and predicted usage, in this case it is set to very short duration for testing purposes
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromSeconds(10));
                Cache.Set(term, synonyms);
                
            }
            return View("/Views/Home/Index.cshtml",new SynonymViewModel { Term = term, Synonyms = synonyms });
        }

        [HttpPost("/addSynonyms")]
        public async Task<IActionResult> AddSynonyms([Required] string term, [Required][RegularExpression(@"^[a-zA-Z0-9 ,]+$")] string synonyms)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entry = new SynonymEntryModel { Term = term, Synonyms = synonyms };
            await DbContext.SynonymEntries.AddAsync(entry);
            await DbContext.SaveChangesAsync();

            UpdateCacheIfNeeded(term, synonyms.Split(','));

            return await Synonyms(term);
        }

        private void UpdateCacheIfNeeded(string term, IEnumerable<string> synonyms)
        {
            //update cache if new synonym was added
            if (Cache.TryGetValue(term, out HashSet<string> cachedSynonyms) && synonyms.Any(x => !cachedSynonyms.Contains(x)))
            {
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromSeconds(10));
                var newSynonyms = cachedSynonyms.Union(synonyms.ToHashSet());
                Cache.Set(term, newSynonyms);
            }
            foreach (var synonym in synonyms)
            {
                if (Cache.TryGetValue(synonym, out cachedSynonyms) && !cachedSynonyms.Contains(term))
                {
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        .SetSlidingExpiration(TimeSpan.FromSeconds(10));
                    cachedSynonyms.Add(term);
                    Cache.Set(synonym, cachedSynonyms);
                }
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
